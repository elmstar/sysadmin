<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enterprise extends Model
{
    public function branches() {
        return $this->hasMany('\App\Branch');
    }
}
