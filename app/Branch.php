<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    public function departments() {
        return $this->hasMany('\App\Department');
    }
    public function enterprise() {
        return $this->belongsTo('\App\Enterprise');
    }
    public function city() {
        return $this->belongsTo('\App\City');
    }
}
