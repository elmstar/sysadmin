<?php

namespace App\Http\Controllers;
use \App\City;
use \App\Country;
use Illuminate\Http\Request;

class Geo extends Controller
{
    public function allCountries(Request $request) {
        $uri = $request->segments();
        $countries = Country::all();
        return view('geo.countries', ['countries'=>$countries, 'uri'=>$uri]);
    }
    public function countriesEdit(Request $request, $id = 0) {
        $uri = $request->segments();
        if ($request->isMethod('get')) {
            if ($id == 0) {
                $act = 'new';
                $country = '';
            } else {
                $act = 'edit';
                $country = Country::findOrFail($id);
            }
            return view('geo.countriesEdit', ['country'=>$country, 'act'=>$act, 'uri'=>$uri]);
        } else {
            if ($id == 0) {
                $country = new Country;
            } else {
                $country = Country::findOrFail($id);
            }
            $country->name = $request->name;
            $country->save();
            return redirect('/geo/countries');
        }
    }
    public function allCities(Request $request) {
        $uri = $request->segments();
        $cities = City::all();
        return view('geo.cities', ['cities'=>$cities, 'uri'=>$uri]);
    }
    public function citiesEdit(Request $request, $id = 0) {
        $uri = $request->segments();
        if ($request->isMethod('get')) {
            if ($id == 0) {
                $act = 'new';
                $city = '';
            } else {
                $act = 'edit';
                $city = City::findOrFail($id);
            }
            $countries = Country::all();
            return view('geo.citiesEdit', ['city'=>$city, 'countries'=>$countries, 'act'=>$act, 'uri'=>$uri] );
        } else {
            if ($id == 0) {
                $city = new City;
            } else {
                $city = City::findOrFail($id);
            }
            $city->name = $request->name;
            $city->country_id = $request->country_id;
            $city->save();
            return redirect('/geo/cities');
        }
    }
}
