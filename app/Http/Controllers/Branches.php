<?php

namespace App\Http\Controllers;
use \App\Branch;
use \App\City;
use \App\Enterprise;
use Illuminate\Http\Request;

class Branches extends Controller
{
    public function all($entID = 0,Request $request) {
        $branches = Branch::with('enterprise','city')->paginate(15);
        $uri = $request->segments();
        if (!isset($uri[2]))
            $uri[2] = 0;
        return view('ent.branches', ['branches'=>$branches, 'uri'=>$uri]);
    }
    public function edit(Request $request, $id = 0) {
        $uri = $request->segments();
        if ($request->isMethod('get')) {
            if ($id == 0) {
                $act = 'new';
                $branch = '';
            } else {
                $act = 'edit';
                $branch = Branch::findOrFail($id);
            }
            $enterprises = Enterprise::all();
            $cities = City::all();

            return view('ent.branchesEdit', ['branch'=>$branch,'cities'=>$cities,'enterprises'=>$enterprises, 'act'=>$act,'uri'=>$uri]);
        } else {
            if ($id == 0) {
                $branch = new Branch;
            } else {
                $branch = Branch::findOrFail($id);
            }
            $branch->name = $request->name;
            $branch->enterprise_id = $request->enterprise_id;
            $branch->city_id = $request->city_id;
            $branch->save();
            return redirect('/ent/branches'); //
        }

    }
}
