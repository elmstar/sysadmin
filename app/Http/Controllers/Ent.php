<?php

namespace App\Http\Controllers;
use App\Enterprise;
use App\Branch;
use App\City;
use Illuminate\Http\Request;

class Ent extends Controller
{
    public function welcome(Request $request) {
        $uri = ['','',''];
        return view('welcome', ['uri'=>$uri]);
    }
    public function all(Request $request)

    {
        $enterprises = Enterprise::all();
        $position = 0;
        $uri = $request->segments();
        return view('ent.enterprises', ['enterprises'=>$enterprises, 'position'=>$position, 'uri'=>$uri]);
    }
    public function edit(Request $request, $id = 0) {
        $uri = $request->segments();
        if ($request->isMethod('get')) {
            if ($id == 0) {
                $act = 'new';
                $enterprise = null;
            } else {
                $act = 'edit';
                $enterprise = Enterprise::findOrFail($id);
            }
            $cities = City::all();
            return view('ent.enterpriseEdit', ['enterprise'=>$enterprise,'cities'=>$cities, 'act'=>$act, 'uri'=>$uri]);
        } else {
            if($request->act == 'new') {
                $enterprise = new Enterprise;
                $branch = new Branch;
                $branch->name = 'Центральный офис';
                $branch->city_id = $request->city;
            } else {
                $enterprise = Enterprise::findOrFail($id);
            }
            $enterprise->name = $request->name;
            $enterprise->priority = $request->priority;
            $enterprise->save();
            if ($act = 'new') {
                $branch->enterprise_id = $enterprise->id;
                $branch->save();
            }
            return redirect('/ent/ents');
        }
    }
}
