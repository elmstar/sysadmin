<?php

namespace App\Http\Controllers;
use \App\Department;
use Illuminate\Http\Request;

class Departments extends Controller
{
    public function all(Request $request) {
        $uri = $request->segments();
        $departments = Department::all();
        return view('ent.departments', ['departments'=>$departments, 'uri'=>$uri]);
    }
}
