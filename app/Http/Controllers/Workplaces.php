<?php

namespace App\Http\Controllers;
use \App\Workplace;
use Illuminate\Http\Request;

class Workplaces extends Controller
{
    public function all(Request $request) {
        $workplaces = Workplace::all();
        $uri = $request->segments();
        return view('ent.workplaces', ['workplaces'=>$workplaces, 'uri'=>$uri]);
    }
}
