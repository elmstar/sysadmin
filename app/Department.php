<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public function workplaces() {
        return $this->hasMany('\App\Workplace');
    }
    public function branch() {
        return $this->belongsTo('\App\Branch');
    }
}
