@extends('tmp.tmp')
@if ($act == 'new')
    @section('title', 'Новый город')
@else
    @section('title', 'Поправки в городе')
@endif


@section('main')
    @if ($act == 'new')
        <h1>Новый город</h1>
    @else
        <h1>Поправки в городе</h1>
    @endif
    <form action="" method="POST">
        {{ csrf_field() }}
        <table>
            <tr><td>Название:</td><td><input name="name" value="{{ $city->name??'' }}"></td></tr>
            <tr><td>Страна:</td><td>
                    <select name="country_id">
                        @foreach ($countries AS $country)
                        <option
                                @if (isset($city->country->id) AND $country->id == $city->country->id)
                                    selected
                                @endif
                                value="{{$country->id}}"
                                >
                                {{$country->name}}</option>


                        @endforeach
                    </select>
                </td></tr>
            <tr><td><input type="hidden" mane="act" value="{{$act}}"></td><td><input type="submit"></td></tr>
        </table>

    </form>

@endsection
