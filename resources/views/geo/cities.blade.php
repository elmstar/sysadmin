@extends('tmp.tmp')
@section('title', 'Города')
@section('main')
    <table>
        <thead>
        <th>Название</th><th>Страна</th>
        @foreach ($cities AS $city)
            <tr>
                <td>{{$city->name}}</td>
                <td>{{$city->country->name}}</td>
                <td><a href="/geo/cities/edit/{{$city->id}}">Правка</a></td>
            </tr>
        @endforeach
        <tr><td><a href="/geo/cities/new">Новый</a></td></tr>
        </thead>
    </table>
@endsection