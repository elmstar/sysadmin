@extends('tmp.tmp')
@section('title', 'Страны')
@section('main')
    <table>
        <thead>
        <th>Название</th><th>кол-во городов</th>
        @foreach ($countries AS $country)
            <tr><td>{{$country->name}}</td><td>{{count($country->cities)}}</td><td><a href="/geo/countries/edit/{{$country->id}}">Правка</a></td></tr>
        @endforeach
        <tr><td><a href="/geo/countries/new">Новая</a></td></tr>
        </thead>
    </table>
@endsection