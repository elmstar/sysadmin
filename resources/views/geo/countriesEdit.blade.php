@extends('tmp.tmp')
@if ($act == 'new')
    @section('title', 'Новая страна')
@else
    @section('title', 'Поправки в стране')
@endif


@section('main')
    @if ($act == 'new')
        <h1>Новая страна</h1>
    @else
        <h1>Поправки в стране</h1>
    @endif
    <form action="" method="POST">
        {{ csrf_field() }}
        <table>
            <tr><td>Название:</td><td><input name="name" value="{{ $country->name??'' }}"></td></tr>
            <tr><td><input type="hidden" name="act" value="{{$act}}"></td><td><input type="submit"></td></tr>
        </table>

    </form>

@endsection