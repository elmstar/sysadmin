@extends('tmp.tmp')
@if ($act == 'new')
    @section('title', 'Новый филиал')
@else
    @section('title', 'Поправки в филиале')
@endif
@section('main')
    @if ($act == 'new')
        <h1>Добавить филиал</h1>
    @else
        <h1>Поправки в филиале</h1>
    @endif
    <form action="" method="POST">
    {{ csrf_field() }}
        <table>
            <tr><td>Название: </td><td><input name="name" value="{{$branch->name??''}}"</td></tr>
            <tr><td>Город:</td><td>
                    <select name="city_id">
                        @foreach ($cities AS $city)
                            <option
                                    @if (isset($branch->city->id) AND $branch->city->id == $city->id)
                                            selected
                                    @endif
                                    value="{{$city->id}}">{{$city->name}}</option>
                        @endforeach
                    </select>
                </td></tr>
            <tr><td>Предприятие:</td><td>
                    <select name="enterprise_id">
                        @foreach ($enterprises AS $enterprise)
                            <option
                                    @if (isset($branch->$enterprise->id) AND $branch->$enterprise->id == $enterprise->id)
                                            selected
                                    @endif

                                    value="{{$enterprise->id}}">{{$enterprise->name}}</option>
                        @endforeach
                    </select>

                </td>
            </tr>
            <tr><td><input type="hidden" name="act" value="{{$act}}"></td><td><input type="submit"></td></tr>
        </table>

    </form>

@endsection