@extends('tmp.tmp')
@section('title', 'Филиалы всех фирм')
@section('main')
    @if ($uri[2]>0)
        <h1>Филиалы фирмы {{ $branches->where('enterprise_id',$uri[2])->first()->enterprise->name }}</h1>
    @else
        <h1>Филиалы всех фирм</h1>
    @endif
<table>
    <thead>
    <th>Название</th><th>Город</th>
    @if ($uri[2] == 0)
    <th>Предприятие</th>
    @endif
    @foreach ($branches AS $branch)
        @if ($uri[2] == 0 OR $uri[2] == $branch->enterprise->id)
        <tr>
            <td>{{$branch->name}}</td>
            <td>{{$branch->city->name}}</td>
            @if ($uri[2] == 0)
            <td>{{$branch->enterprise->name}}</td>
            @endif
            <td><a href="/ent/branches/edit/{{$branch->id}}">Правка</a></td>
        </tr>
        @endif
    @endforeach
    <tr><td><a href="/ent/branches/new">Новый</a></td></tr>
    </thead>
</table>
@endsection