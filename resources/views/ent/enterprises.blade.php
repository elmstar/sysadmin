@extends('tmp.tmp')
@section('title', 'Предприятия')
@section('main')
    <h1>Список предприятий</h1>
    <table>
        <thead>
        <th>#</th><th>Название</th><th>Приоритет</th><th>Действия</th>
        </thead>
        @foreach($enterprises AS $enterprise)
        <tr>
            <td>{{$enterprise->id}}</td>
            <td>{{$enterprise->name}}</td>
            <td>{{$enterprise->priority}}</td>
            <td><a href="/ent/ents/edit/{{$enterprise->id}}">Правка</a>&bull;
                <a href="/ent/branches/{{$enterprise->id}}">Филиалы</a>
            </td>
        </tr>
        @endforeach
        <tr><td><a href="/ent/ents/new">Новое</a></td><td></td><td></td></tr>
    </table>
@endsection