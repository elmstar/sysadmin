@extends('tmp.tmp')
@if ($act == 'new')
    @section('title', 'Новая компания')
@else
    @section('title', 'Поправки в компании')
@endif
@section('main')
    @if ($act == 'new')
        <h1>Добавить компанию</h1>
    @else
        <h1>Поправки в компании</h1>
    @endif
    <form action="" method="POST">
        {{ csrf_field() }}
        <table>
            <tr><td>Название: </td><td><input name="name" value="{{$enterprise->name??''}}"></td></tr>
            @if ($act == 'new')
                <tr><td>Город:</td><td><select name="city">
                            @foreach ($cities AS $city)
                                <option value="{{$city->id}}">{{$city->name}}</option>
                            @endforeach
                        </select>

                    </td></tr>
            @endif
            <tr><td>Приоритет: </td><td><select name="priority">
                        @for ($i=0; $i<7; $i++)
                        <option
                                @if (is_object($enterprise) AND $enterprise->priority == $i)
                                        selected
                                @endif
                                value="{{$i}}"> {{$i}} </option>
                        @endfor
                    </select></td></tr>
            <tr><td><input type="hidden" name="act" value="{{$act}}"></td><td><input type="submit"></td></tr>
        </table>

    </form>
@endsection