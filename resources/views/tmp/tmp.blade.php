<!DOCTYPE html>
<html lang="ru">
<head>
    <title>@yield('title')</title>
    <link rel="stylesheet" href="/main.css">
</head>
<body>
<nav>
    <ul>
        <li><a href="/">Главная</a></li>
    </ul>
        <hr/>
    <ul>
        <li><a href="/ent/ents">Предприятие</a></li>
        <li><a href="/geo/countries">География</a></li>
        <li><a href="/value">Средства</a></li>
        <li><a href="/users">Пользователи</a></li>
    </ul>

</nav>
<article>
    <div class="header-menu">
        <table><tr>
                @if ($uri[0] == 'ent')
                    <td
                    @if ($uri[1] == 'ents')
                        class="checked"
                    @endif
                    ><a href="/ent/ents">Предприятия</a></td>

                    <td
                            @if ($uri[1] == 'branches')
                            class="checked"
                            @endif
                    ><a href="/ent/branches">Филиалы</a></td>

                    <td
                            @if ($uri[1] == 'deps')
                            class="checked"
                            @endif
                    ><a href="/ent/deps">Отделы</a></td>

                    <td
                            @if ($uri[1] == 'works')
                            class="checked"
                            @endif
                    ><a href="/ent/works">Рабочие места</a></td>
                @endif
                @if ($uri[0] == 'geo')
                        <td
                                @if ($uri[1] == 'countries')
                                class="checked"
                                @endif
                        ><a href="/geo/countries">Страны</a></td>
                        <td
                                @if ($uri[1] == 'cities')
                                class="checked"
                                @endif
                        ><a href="/geo/cities">Города</a></td>
                @endif
            </tr></table>

    </div>{{-- class="header-menu" --}}
@yield('main')
</article>
</body>
</html>
