<?php

Route::get('/', 'Ent@welcome'); // стартовая страница
Route::get('/ent/ents', 'Ent@all');
Route::match(['get','post'],'/ent/ents/new', 'Ent@edit');
Route::match(['get','post'],'/ent/ents/edit/{id}','Ent@edit')->where('id', '[0-9]+');
Route::get('/ent/branches', 'Branches@all');
Route::match(['get','post'],'/ent/branches/new','Branches@edit');
Route::match(['get','post'],'/ent/branches/edit/{id}','Branches@edit');
Route::get('/ent/branches/{entID}', 'Branches@all')->where('entID', '[0-9]+');

Route::get('/ent/deps', 'Departments@all');

Route::get('/ent/works', 'Workplaces@all');

Route::get('/geo/countries', 'Geo@allCountries');
Route::match(['get', 'post'], '/geo/countries/new', 'Geo@countriesEdit');
Route::match(['get', 'post'], '/geo/countries/edit/{id}', 'Geo@countriesEdit');

Route::get('/geo/cities', 'Geo@allCities');
Route::match(['get', 'post'], '/geo/cities/new', 'Geo@citiesEdit');
Route::match(['get', 'post'], '/geo/cities/edit/{id}', 'Geo@citiesEdit');

Route::get('/value', 'Values@all');

Route::get('/users', 'Users@all');
